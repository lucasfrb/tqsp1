/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.api;


import deti.tqs.p1.tqsp1.controllers.PlaceController;


import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author lbarros
 */
@RunWith(SpringRunner.class)
@WebMvcTest(PlaceController.class)
public class TestPlaceController {
    
    @Autowired
    private MockMvc mvc;
    
    
    
    @Test
    public void testGetPrevision_good() throws Exception{
        String name = "Aveiro";
        int days = 5;
        
        
        
        mvc.perform(get("/places/" + name + "/" + days)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(days)))
        .andExpect(jsonPath("$[1].forecastDate", is("Amanhã")))
        .andExpect(jsonPath("$[0].forecastDate", is("hoje")));
          
        
        
    }
    
    @Test
    public void testGetPrevision_good2() throws Exception{
        String name = "Braga";
        int days = 2;
        
        
        
        mvc.perform(get("/places/" + name + "/" + days)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(days)))
        .andExpect(jsonPath("$[1].forecastDate", is("Amanhã")))
        .andExpect(jsonPath("$[0].forecastDate", is("hoje")));
        
      
        
        
    }
    
    @Test(expected = FileNotFoundException.class)
    public void testGetPrevision_bad() throws Exception{
        String name = "nenhuma cidade";
        int days = 5;
        
        
        
        mvc.perform(get("/places/" + name + "/" + days)
        .contentType(MediaType.APPLICATION_JSON))
        
        .andExpect(status().isBadRequest());
        
        
        
    }
    
    
    @Test
    public void testGetPlaces() throws Exception{
        int numberPlaces = 30;
        
        mvc.perform(get("/places/")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(numberPlaces)))
        .andExpect(jsonPath("$[0].name", is("Aveiro")));
    }

    
    
}
