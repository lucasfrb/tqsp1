/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.junit;


import deti.tqs.p1.tqsp1.entitys.Prevision;

import java.util.ArrayList;
import java.util.List;

import deti.tqs.p1.tqsp1.memory.Cache;
import deti.tqs.p1.tqsp1.memory.PlacePrevision;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
import static org.hamcrest.CoreMatchers.*;

/**
 *
 * @author lbarros
 */
@RunWith(MockitoJUnitRunner.class)

public class CacheTest {
    
    
    Cache cache;
    
    @Mock
    PlacePrevision pp;
    
    @Mock Prevision p;
    
    
    
    
    @Before
    public void setUp() {
        cache = new Cache();
        pp = mock(PlacePrevision.class);
        p = mock(Prevision.class);
    }
    

    /**
     * Test of add method, of class Cache.
     */
    @Test
    public void testAdd() {
        cache.add(pp);
        assertEquals(1, cache.getSize());
        
    }
    
    
    
    @Test
    public void testRemove(){
        
        cache.add(pp);
        cache.remove(pp);
        assertEquals(0, cache.getSize());
    }
    
    
    @Test
    public void testHasPrevision(){
        when(pp.getLocalId()).thenReturn(1);
        when(pp.getDays()).thenReturn(4);
        when(pp.startToday()).thenReturn(Boolean.TRUE);
        cache.add(pp);
        assertEquals(true, cache.hasPrevision(1, 3));
    }
    
    @Test
    public void testGetPrevisions(){
        when(pp.getLocalId()).thenReturn(1);
      
        List<Prevision> aux = new ArrayList<>();
        aux.add(p);
        
        when(pp.getPrevisions(1)).thenReturn(aux);
        cache.add(pp);
        assertEquals(cache.getPrevisions(1, 1), aux);
        
    }
    
    @Test
    public void testHasRecord_good(){
        int local_id = 110005;
        int days = 1;
        String url = "places/110005/1";
        HashMap<String ,int[]> stats = new HashMap<>();
        stats.put(url, new int [] {0, 0});
        cache.setStatistics(stats);
        assertEquals(true, cache.hasRecord(local_id, days));
    }
    
    @Test
    public void testHasRecord_bad(){
        int local_id = 110005;
        int days = 1;
        String url = "places/110005/1";
        HashMap<String ,int[]> stats = new HashMap<>();
        stats.put(url, new int [] {0, 0});
        cache.setStatistics(stats);
        assertEquals(false, cache.hasRecord(11000555, days));
    }
    
    @Test
    public void testCreateRecord(){
        int local_id = 1111;
        int days = 4;
        
        cache.createRecord(local_id, days);
        assertEquals(true, cache.hasRecord(local_id, days));
        assertEquals(1, cache.getStatistics().size());
        
    }
    
    @Test
    public void updateHits(){
        int local_id = 1111;
        int days = 4;
        String url = "places/" + local_id + "/" + days;
        cache.createRecord(local_id, days);
        cache.updateHits(local_id, days);
        assertEquals(1, cache.getStatistics().get(url)[0]);
        assertEquals(0, cache.getStatistics().get(url)[1]);
    }
    
    @Test
    public void updateMisses(){
        int local_id = 1111;
        int days = 4;
        String url = "places/" + local_id + "/" + days;
        cache.createRecord(local_id, days);
        cache.updateMisses(local_id, days);
        assertEquals(1, cache.getStatistics().get(url)[1]);
        assertEquals(0, cache.getStatistics().get(url)[0]);
    }
    
    @Test
    public void testGetSize(){
        cache.add(pp);
        assertEquals(1, cache.getSize());
    }

    
    @Test
    public void testClean(){
        cache.add(pp);
        assertThat(cache.getSize(), not(0));
        cache.clean();
        assertEquals(0, cache.getSize());
    }
    
    
    
    
}
