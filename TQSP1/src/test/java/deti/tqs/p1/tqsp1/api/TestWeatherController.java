/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.api;

import deti.tqs.p1.tqsp1.controllers.WeatherController;
import deti.tqs.p1.tqsp1.entitys.WeatherDesc;
import deti.tqs.p1.tqsp1.Processor;


import java.util.HashMap;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



/**
 *
 * @author lbarros
 */

@RunWith(SpringRunner.class)
@WebMvcTest(WeatherController.class)
public class TestWeatherController {
    
    @Autowired
    private MockMvc mvc;
       
    
    
    @Test
    public void testGet() throws Exception{
        int id = 1;
        
        mvc.perform(get("/weather/" + id)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.url", is("sunny.jpg")));

    }
    
    @Test
    public void testGet2() throws Exception{
        int id = 6;
        
        mvc.perform(get("/weather/" + id)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(6)))
        .andExpect(jsonPath("$.url", is("cloudy_raining.jpg")));

    }
    
    @Test
    public void testGet_bad() throws Exception{
        int id = 40;
        
        mvc.perform(get("/weather/" + id)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        ;
        

    }
    
    
}
