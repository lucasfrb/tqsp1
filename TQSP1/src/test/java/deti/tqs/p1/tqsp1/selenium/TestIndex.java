/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.selenium;

import deti.tqs.p1.tqsp1.htmlpages.Index;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestIndex {
  private WebDriver driver;
  private Index index;

  @Before
  public void setUp() throws Exception {
    driver = new ChromeDriver(new ChromeOptions().addArguments("--headless"));
    index = new Index(this.driver);
  }

  @Test
  public void testAveiro3() throws Exception {
    
    index.selectDistrict("Aveiro");
    index.selectDays("3");
    index.submit();
    assertTrue(driver.findElement(By.id("titulo")).isDisplayed());
    String text = "Aveiro";
    assertTrue(index.getTitle().contains(text));
    System.out.println("number of rows : " + index.getNumberRows());
    assertTrue(index.getNumberRows() == 3);
  }
  
  
  
  
  @Test
  public void testBraga4() throws Exception{
      
      index.selectDistrict("Braga");
      index.selectDays("4");
      index.submit();
      
      assertTrue(driver.findElement(By.id("titulo")).isDisplayed());
      String text = "Braga";
      System.out.println(index.getTitle());
      assertTrue(index.getTitle().contains(text));
      System.out.println("number of rows : " + index.getNumberRows());
      assertTrue(index.getNumberRows() == 4);
  }
  
  @Test
  public void testCities(){
      List<WebElement> lista = index.getCities();
      List<String> citys = new ArrayList<>();
      for (WebElement we : lista)
          citys.add(we.getText());
      assertTrue(citys.contains("Aveiro"));
      assertTrue(citys.contains("Braga"));
      assertTrue(citys.contains("Porto"));
      assertTrue(citys.contains("Lisboa"));
      assertTrue(citys.contains("Santarém"));
      assertTrue(citys.contains("Setúbal"));
      assertTrue(citys.contains("Viana do Castelo"));
      assertTrue(citys.contains("Vila Real"));
      
  }

  @After
  public void tearDown() throws Exception {
    driver.close();
    
  }

  

 
}
