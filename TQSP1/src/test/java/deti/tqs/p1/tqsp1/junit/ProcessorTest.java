/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.junit;

import com.fasterxml.jackson.databind.ObjectMapper;
import deti.tqs.p1.tqsp1.Connector;
import deti.tqs.p1.tqsp1.entitys.Place;
import deti.tqs.p1.tqsp1.Processor;
import java.io.IOException;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 *
 * @author lbarros
 */

@RunWith(MockitoJUnitRunner.class)
public class ProcessorTest {
    
     Processor proc;
    
    
    @Before
    public void setup(){
        proc = new Processor();
    }
    
    
    @Test
    public void testGetPlaces() throws IOException, JSONException{
        assertEquals(30, proc.getPlaces().size());
        assertEquals("Place{id=1010500, name=Aveiro}", proc.getPlaces().get(0).toString());
        assertEquals("Place", proc.getPlaces().get(0).getClass().getSimpleName());
    }
    
    @Test
    public void testGetWeather() throws IOException, JSONException{
        assertEquals(29, proc.getWeather().size());
        assertEquals("WeatherDesc{id=-99, PTdesc=---, ENdesc=--}", proc.getWeather().get(0).toString());
        assertEquals("WeatherDesc", proc.getWeather().get(0).getClass().getSimpleName());
    }
    
    @Test
    public void testGetLocal() throws IOException, JSONException{
         Place p = proc.getPlaces().get(0);
         JSONObject obj = proc.getLocal(p.getId());
         assertEquals(5, obj.getJSONArray("data").length());
         assertEquals("IPMA", obj.getString("owner"));
         assertEquals("PT", obj.getString("country"));
                 
    
    }
    
}
