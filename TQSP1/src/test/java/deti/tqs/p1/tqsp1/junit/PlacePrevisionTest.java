/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.junit;

import deti.tqs.p1.tqsp1.entitys.Prevision;
import deti.tqs.p1.tqsp1.memory.PlacePrevision;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;

/**
 *
 * @author lbarros
 */

@RunWith(MockitoJUnitRunner.class)
public class PlacePrevisionTest {
    
    PlacePrevision pp;
    
    @Mock
    Prevision p;
    
    @Mock
    Prevision p2;
    
    @Before
    public void setup(){
        pp = new PlacePrevision();
    }
    
    
    @Test 
    public void testAdd(){
        pp.add(0, new Prevision());
        assertEquals(1, pp.getPrevisions().size());
        
    }
    
    @Test
    public void testClean(){
        pp.add(0, new Prevision());
        pp.add(1, new Prevision());
        pp.clean();
        assertEquals(0, pp.getPrevisions().size());
    }
    
    @Test
    public void testStartToday(){
        p = mock(Prevision.class);
        when(p.getForecastDate()).thenReturn("hoje");
        pp.add(0, p);
        assertEquals(true, pp.startToday());
        
    }
    
    @Test
    public void testGetPrevisions(){
        p = mock(Prevision.class);
        p2 = mock(Prevision.class);
     
        
        when(p.getForecastDate()).thenReturn("hoje");
        when(p2.getForecastDate()).thenReturn("amanhã");
        pp.add(0, p);
        pp.add(1, p2);
        pp.add(2, new Prevision());
        assertEquals(2, pp.getPrevisions(2).size());
        assertEquals(3, pp.getPrevisions(3).size());
        assertEquals( "Prevision", pp.getPrevisions(3).get(2).getClass().getSimpleName());
        assertEquals("hoje", pp.getPrevisions(3).get(0).getForecastDate());
        assertEquals("amanhã", pp.getPrevisions(3).get(1).getForecastDate());
    }

    
}

