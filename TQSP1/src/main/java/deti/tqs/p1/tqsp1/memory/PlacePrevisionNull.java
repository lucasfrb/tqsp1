/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.memory;
import deti.tqs.p1.tqsp1.entitys.Prevision;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lbarros
 */
public class PlacePrevisionNull extends PlacePrevision{
    
    public PlacePrevisionNull(){
        this.localId = -1;
        this.previsions = new ArrayList<>();
    }
    
    
    @Override
    public List<Prevision> getPrevisions(int days) {
        return new ArrayList<>();
    }
    
}
