/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.entitys;

/**
 *
 * @author lbarros
 */
public class WeatherDesc {
    
    private int id;
    private String descPT;
    private String descEN;
    private String url;

    public WeatherDesc(int id, String descPT, String descEN) {
        this.id = id;
        this.descPT = descPT;
        this.descEN = descEN;
    }

    public int getId() {
        return id;
    }

    public String getPTdesc() {
        return descPT;
    }

    public String getENdesc() {
        return descEN;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPTdesc(String descPT) {
        this.descPT = descPT;
    }

    public void setENdesc(String descEN) {
        this.descEN = descEN;
    }
    
    

    public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
    public String toString() {
        return "WeatherDesc{" + "id=" + id + ", PTdesc=" + descPT + ", ENdesc=" + descEN + '}';
    }
    
    
    
}
