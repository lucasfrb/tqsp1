/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1;

import deti.tqs.p1.tqsp1.entitys.Place;
import deti.tqs.p1.tqsp1.entitys.WeatherDesc;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

import org.json.JSONObject;

/**
 *
 * @author lbarros
 */
public class Processor {
    
    Connector con;
    
    public Processor(){
        con = new Connector();
    }
    
    public List<Place> getPlaces() throws IOException{
        List<Place> places = new ArrayList<>();
        JSONObject response = con.getPlaces();
        JSONArray data = response.getJSONArray("data");

        for (int i = 0; i < data.length(); i++) {
            JSONObject obj = data.getJSONObject(i);
            String local = obj.getString("local");
            int id = obj.getInt("globalIdLocal");
            Place p = new Place(id, local);
            places.add(p);
        }
        return places;
    }
    
    public List<WeatherDesc> getWeather() throws IOException{
        List<WeatherDesc> weather = new ArrayList<>();
        JSONObject response = con.getDescTempo();
        JSONArray data = response.getJSONArray("data");

        for (int i = 0; i < data.length(); i++) {
            JSONObject obj = data.getJSONObject(i);
            int id = obj.getInt("idWeatherType");
            String descPT = obj.getString("descIdWeatherTypePT");
            String descEN = obj.getString("descIdWeatherTypeEN");
            weather.add(new WeatherDesc(id, descPT, descEN));
        }
        
        return weather;
    }
    
    public JSONObject getLocal(int id) throws IOException{
        return con.getLocal(id);
    }
    
}
