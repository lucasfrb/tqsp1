/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.controllers;



import deti.tqs.p1.tqsp1.entitys.WeatherDesc;
import deti.tqs.p1.tqsp1.Processor;

import java.io.IOException;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author lbarros
 */
@Controller
@RequestMapping("/weather")
public class WeatherController {
    
    private List<WeatherDesc> weather;
    
    private HashMap<String , String> desc;		// desc : url
    
    
    
    
    @PostConstruct
    public void init() throws IOException {
        Processor processor = new Processor();
        weather = processor.getWeather();
        fillHashMap();
        fillWeather();
    }
    
    public void fillHashMap() {
        String cloudyRaining = "cloudy_raining.jpg";
        String sunnyCloudy = "sunny_cloudy.jpg";
    	desc = new HashMap<>(); 
        desc.put("Clear sky", "sunny.jpg");
        desc.put("Sunny intervals", sunnyCloudy);
        desc.put("Cloudy", "cloudy.jpg");
        desc.put("Cloudy (High cloud)", "very_cloudy.jpg");
        desc.put("Showers", cloudyRaining);
        desc.put("Light showers", cloudyRaining);
        desc.put("Heavy showers", cloudyRaining);
        desc.put("Rain", cloudyRaining);
        desc.put("Light rain", "sunny_cloudy_raining.jpg");
        desc.put("Heavy rain", cloudyRaining);
        desc.put("Intermittent rain", cloudyRaining);
        desc.put("Intermittent ligth rain", cloudyRaining);
        desc.put("Intermittent heavy rain", cloudyRaining);
        desc.put("Drizzle", "sunny_cloudy_raining.jpg");
        desc.put("Mist", sunnyCloudy);
        desc.put("Fog", sunnyCloudy);
        desc.put("Snow", "cloudy_snowing.jpg");
        desc.put("Thunderstorms", "cloudy_storming.jpg");
        desc.put("Showers and thunderstorms", "cloudy_storming_raining.jpg");
        desc.put("Hail", "cloudy_running_snowing.jpg");
        desc.put("Frost", "cloudy_running_snowing.jpg");
        desc.put("Rain and thunderstorms", "cloudy_storming_raining.jpg");
        desc.put("Convective clouds", "very_cloudy.jpg");
        desc.put("Partly cloudy", sunnyCloudy);
        
        
    	
    	
    }
    
    public void fillWeather() {
    	for (WeatherDesc w : weather) 
    		w.setUrl(desc.get(w.getENdesc()));
    		
    	
    }
    
    @GetMapping("/{id}")
    public @ResponseBody Optional<WeatherDesc> get(@PathVariable("id") int id){
        WeatherDesc aux = null;
        for (WeatherDesc wd : weather){
            if (wd.getId() == id){
                aux = wd;
            }
                
        }
        
        if (aux != null)
            return Optional.of(aux);
        else
            return Optional.empty();
        
    }
    
    
}
