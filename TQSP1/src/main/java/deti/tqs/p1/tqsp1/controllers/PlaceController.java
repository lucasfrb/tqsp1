/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.controllers;

import deti.tqs.p1.tqsp1.memory.Cache;
import deti.tqs.p1.tqsp1.memory.PlacePrevision;
import deti.tqs.p1.tqsp1.entitys.Place;
import deti.tqs.p1.tqsp1.entitys.Prevision;
import deti.tqs.p1.tqsp1.Processor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author lbarros
 */

@Controller
//@Component
//@EnableScheduling
@RequestMapping("/places")
public class PlaceController {
    
    
    
    private List<Place> places;
    
    private Processor processor;
    private Cache cache;

    @PostConstruct
    public void init() throws IOException {
        cache = new Cache();
        places = new ArrayList<>();
        
        processor = new Processor();
        
        places = processor.getPlaces();
        

    }
    
    @GetMapping("/{name}/{days}")
    @CrossOrigin(origins = "*")
    public @ResponseBody Iterable<Prevision> getPrevision(@PathVariable("name") String name, @PathVariable("days") Optional<Integer> days) throws IOException{
        int localId = 0;
        for (Place p : places){
            if (p.getName().equals(name)){
                localId = p.getId();
            }
        }
        
        int dayss = 0;
        if (days.isPresent())
            dayss = days.get();
        
        if (cache.hasPrevision(localId, dayss)){
            if (!cache.hasRecord(localId, dayss))
                cache.createRecord(localId, dayss);
            cache.updateHits(localId, dayss);
            return cache.getPrevisions(localId, dayss);
        }
            
        PlacePrevision prev = new PlacePrevision();
        prev.setLocalId(localId);
        
        
        JSONObject reponse = processor.getLocal(localId);
        JSONArray data = reponse.getJSONArray("data");
        
        for (int i = 0; i < data.length(); i++){
            JSONObject obj = data.getJSONObject(i);
            String precipitaProb = (String) obj.get("precipitaProb");
            String tMin = (String) obj.get("tMin");
            String tMax = (String) obj.get("tMax");
            String predWindDir = (String) obj.get("predWindDir");
            int idWeatherType = obj.getInt("idWeatherType");
            int classWindSpeed = obj.getInt("classWindSpeed");
            String longitude = (String) obj.get("longitude");
            String forecastDate = (String) obj.get("forecastDate");
            String latitude = (String) obj.get("latitude");
            
            Prevision p = new Prevision(precipitaProb, tMin, tMax, predWindDir, idWeatherType, classWindSpeed, longitude, forecastDate, latitude);
            prev.add(i, p);
        }
        cache.add(prev);
        if (!cache.hasRecord(localId, dayss)){
            cache.createRecord(localId, dayss);
            
        }
        cache.updateMisses(localId, dayss);
        
        return prev.getPrevisions(dayss);
    }
    
    @GetMapping("/statistics")
    public @ResponseBody Map<String, int []> getStatistics(){
        return cache.getStatistics();
    }
    
   
    
    @GetMapping("/")
    public @ResponseBody Iterable<Place> getPlaces(){
        return places;
    }
    
    
    
    
    
}
