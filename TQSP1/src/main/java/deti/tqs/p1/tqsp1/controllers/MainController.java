/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.controllers;

import deti.tqs.p1.tqsp1.Processor;
import deti.tqs.p1.tqsp1.entitys.Place;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 *
 * @author lbarros
 */
@Controller
@RequestMapping("/")
public class MainController {
    
    private List<Place> places;

    @PostConstruct
    public void init() throws IOException {
        Processor proc = new Processor();
        places = proc.getPlaces();
        

    }
    
    @GetMapping("/")
    @CrossOrigin(origins = "*")
    public String index(Model model){
        model.addAttribute("places", places);
        return "index";
    }
    
    
    
    
    
    

}
