/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import org.json.JSONObject;

/**
 *
 * @author lbarros
 */
public class Connector {

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);
            
        } finally {
            is.close();
            
        }
    }
    
    // My requests
    
    public JSONObject getPlaces() throws IOException{
        return readJsonFromUrl("http://api.ipma.pt/open-data/distrits-islands.json");
    }
    
    public JSONObject getLocal(int localId) throws IOException{
        return readJsonFromUrl(String.format("http://api.ipma.pt/open-data/forecast/meteorology/cities/daily/%d.json", localId));
    }
    
    public JSONObject getDescTempo() throws IOException{
        return readJsonFromUrl("http://api.ipma.pt/open-data/weather-type-classe.json");
    }

    

}
