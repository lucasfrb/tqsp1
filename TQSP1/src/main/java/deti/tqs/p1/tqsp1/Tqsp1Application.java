package deti.tqs.p1.tqsp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tqsp1Application {

	public static void main(String[] args) {
		SpringApplication.run(Tqsp1Application.class, args);
	}

}
