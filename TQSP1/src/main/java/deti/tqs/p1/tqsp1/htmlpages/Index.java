/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.htmlpages;


import org.apache.log4j.Logger ;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author lbarros
 */
public class Index {
    
    private static final  String ERROR = "ERROR! :";
    private static final  Logger LOGGER = Logger.getLogger(Index.class);
    private WebDriver driver;
    

    public Index(WebDriver driver) {
        this.driver = driver;
       
        
        String baseUrl = "http://localhost:8080";
        driver.get(baseUrl + "/");
    }
    
    @SuppressWarnings("empty-statement")
    public void selectDistrict(String district){
        try{
            new Select(driver.findElement(By.id("place"))).selectByVisibleText(district);
            
        } catch(Exception e){
            LOGGER.error(ERROR + e.getMessage());
        }
    }
    
    @SuppressWarnings("empty-statement")
    public void selectDays(String days){
        try{
            new Select(driver.findElement(By.id("days"))).selectByVisibleText(days);
            
        } catch(Exception e){
            LOGGER.error(ERROR + e.getMessage());
        }
    }
    
    public void submit(){
        try{
            WebElement submeter = driver.findElement(By.id("submit"));
            submeter.click();
        } catch(Exception e){
            LOGGER.error(ERROR + e.getMessage());
        }
    }
    
    public String getTitle(){
        try{
            WebElement element = driver.findElement(By.id("titulo"));
            return element.getText();
        } catch(Exception e){
            LOGGER.error(ERROR + e.getMessage());
            return "no";
        }
    }
    
    public int getNumberRows(){
        try{
            return driver.findElements(By.xpath("//table[@id='tabela']/tbody/tr")).size();
            
        } catch(Exception e){
            LOGGER.error(ERROR + e.getMessage());
            return -1;
        }
    }
    
    public List<WebElement> getCities(){
        Select select = new Select(driver.findElement(By.id("place")));
        return select.getOptions();
    }
    
    
}
