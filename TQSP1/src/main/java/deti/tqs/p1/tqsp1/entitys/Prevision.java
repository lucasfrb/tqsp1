/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.entitys;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 *
 * @author lbarros
 */
public class Prevision {
    
    
    private String precipitaProb;
    private String tMin;
    private String tMax;
    private String predWindDir;
    private int idWeatherType;
    private int classWindSpeed;
    private String longitude;
    private String forecastDate;
    private String latitude;
    
    public Prevision() {}
    
    public Prevision(String tmin, String tmax){
    	this.tMin = tmin;
    	this.tMax = tmax;
    }

    public Prevision(String precipitaProb, String tMin, String tMax, String predWindDir, int idWeatherType, int classWindSpeed, String longitude, String forecastDate, String latitude) {
        this.precipitaProb = precipitaProb;
        this.tMin = tMin;
        this.tMax = tMax;
        this.predWindDir = predWindDir;
        this.idWeatherType = idWeatherType;
        this.classWindSpeed = classWindSpeed;
        this.longitude = longitude;
        LocalDate aux = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
        String today = formatter.format(aux);
        String tomorrow = formatter.format(aux.plusDays(1));
        if (today.equals(forecastDate)){
            this.forecastDate = "hoje";
        }
        
        else if (tomorrow.equals(forecastDate)){
            this.forecastDate = "Amanhã";
        }

        else{
            this.forecastDate = forecastDate;
        }
        
        this.latitude = latitude;
        
    }
    
    

    public String getPrecipitaProb() {
        return precipitaProb;
    }

    public String gettMin() {
        return tMin;
    }

    public String gettMax() {
        return tMax;
    }

    public String getPredWindDir() {
        return predWindDir;
    }

    public int getIdWeatherType() {
        return idWeatherType;
    }

    public int getClassWindSpeed() {
        return classWindSpeed;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getForecastDate() {
        return forecastDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setPrecipitaProb(String precipitaProb) {
        this.precipitaProb = precipitaProb;
    }

    public void settMin(String tMin) {
        this.tMin = tMin;
    }

    public void settMax(String tMax) {
        this.tMax = tMax;
    }

    public void setPredWindDir(String predWindDir) {
        this.predWindDir = predWindDir;
    }

    public void setIdWeatherType(int idWeatherType) {
        this.idWeatherType = idWeatherType;
    }

    public void setClassWindSpeed(int classWindSpeed) {
        this.classWindSpeed = classWindSpeed;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setForecastDate(String forecastDate) {
        this.forecastDate = forecastDate;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Prevision{" + "precipitaProb=" + precipitaProb + ", tMin=" + tMin + ", tMax=" + tMax + ", predWindDir=" + predWindDir + ", idWeatherType=" + idWeatherType + ", classWindSpeed=" + classWindSpeed + ", longitude=" + longitude + ", forecastDate=" + forecastDate + ", latitude=" + latitude + "}\n";
    }
    
    
    
}
