package deti.tqs.p1.tqsp1.memory;

import java.util.ArrayList;
import java.util.List;

import deti.tqs.p1.tqsp1.entitys.Prevision;
import java.util.HashMap;
import java.util.Map;




public class Cache {
	
	private final List<PlacePrevision> previsions;
        private HashMap<String, int [] > statistics;
        private static final  int MAXSIZE = 10;
        private static final  String BASEURL = "places/";
	
	public Cache(){
		previsions = new ArrayList<>();
                statistics = new HashMap<>();
	}
		
	public void add(PlacePrevision p) {
            if (this.previsions.size() == MAXSIZE)
                previsions.clear();
            if (!this.hasPrevision(p.getLocalId(), p.getDays()))
                previsions.add(p);
	}
	
	public boolean remove(PlacePrevision p) {
		if (previsions.contains(p)) {
			previsions.remove(p);		
			return true;
		}
		return false;
		
	}
	
	
	public boolean hasPrevision(int localId, int days){
		for (PlacePrevision p : previsions){
			if (p.getLocalId() == localId && p.getDays() >= days && p.startToday()) {
				return true;  
			}
		}
		return false; 
	}
	
	public List<Prevision> getPrevisions(int localId, int days){
		PlacePrevision ret = null;
		for (PlacePrevision p : previsions){
			if (p.getLocalId()== localId){
				ret = p;
			}
		}
		if (ret == null)
                    ret = new PlacePrevisionNull();
		return ret.getPrevisions(days);
	}
        
        public boolean hasRecord(int localId, int days){
            return statistics.containsKey(BASEURL + localId + "/" + days);
        }
        
        public void createRecord(int localId, int days){
            String url = BASEURL + localId + "/" + days;
            statistics.put(url, new int[] {0, 0});
        }
        
        public void updateHits(int localId, int days){
            int hits = statistics.get(BASEURL + localId + "/" + days)[0];
            int misses = statistics.get(BASEURL + localId + "/" + days)[1];
            statistics.remove(BASEURL + localId + "/" + days);
            int [] ret = {++hits, misses};
            statistics.put(BASEURL + localId + "/" + days, ret);
        }
        
        public void updateMisses(int localId, int days){
            int hits = statistics.get(BASEURL + localId + "/" + days)[0];
            int misses = statistics.get(BASEURL + localId + "/" + days)[1];
            statistics.remove(BASEURL + localId + "/" + days);
            int [] ret = {hits, ++misses};
            statistics.put(BASEURL + localId + "/" + days, ret);
        }
        
        public int getSize(){
            return this.previsions.size();
        }
        
        public void setStatistics(Map<String, int[]> stats){
            this.statistics = (HashMap<String, int[]>) stats;
        }
        
        public List<PlacePrevision> getPrevisions(){
            return this.previsions;
        }
	
        public Map<String, int []> getStatistics(){
            return this.statistics;
        }
        
	public void clean(){
		this.previsions.clear();
	}

	@Override
	public String toString() {
		return "Cache [previsions=" + previsions + "]";
	}
        
        
	
	

}
