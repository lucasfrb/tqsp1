/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deti.tqs.p1.tqsp1.memory;

import deti.tqs.p1.tqsp1.entitys.Prevision;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lbarros
 */
public class PlacePrevision {
    
    protected int localId;
    protected List<Prevision> previsions;
    
    
    public PlacePrevision(){
        previsions = new ArrayList<>();
    }
    
    public PlacePrevision(int localId){
        this.localId = localId;
        this.previsions = new ArrayList<>();
    }
   
    
    public void add(int index, Prevision p){
    	previsions.add(index, p);      
    }
    
    public void clean(){
        this.localId = 0;
        this.previsions.clear();
    }
    
    
    public boolean startToday(){
        Prevision p = previsions.get(0);
        return p.getForecastDate().equals("hoje");
    }

    public int getLocalId() {
        return localId;
    }

    public List<Prevision> getPrevisions(int days) {
        
        
        List<Prevision> aux = new ArrayList<>();
        for(int i = 0; i < days; i++){
            aux.add(this.previsions.get(i));
        }
        return aux;
    }


    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public void setPrevisions(List<Prevision> previsions) {
        this.previsions = previsions;
    }
    
    public int getDays() {
    	return this.previsions.size();
    	
    }
    
    public List<Prevision> getPrevisions(){
        return this.previsions;
    }

	@Override
	public String toString() {
		return "PlacePrevision [id_local=" + localId + ", previsions=" + previsions + "]";
	}
    
    
    
    
    
    
}
